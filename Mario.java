import greenfoot.*;
import java.util.List;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Mario here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Mario extends Actor
{
    double gravity = 1;
    double fall;
    
    int vSpeed = 0;
    int acceleration = 1;
    GreenfootImage mario = getImage();
    
    /**
     * Act - do whatever the Mario wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        scroll();
        fall();
        checkCollision();
    }
    
    public void scroll()
    {
        List<Actor> actors = getWorld().getObjects(Actor.class);
        actors.remove(this);
        
        if(Greenfoot.isKeyDown("right"))
        {
            for(Actor actor : actors)
            {
                actor.setLocation(actor.getX() - 5, actor.getY());
            }
        }
        
        if(Greenfoot.isKeyDown("left"))
        {
            for(Actor actor : actors)
            {
                actor.setLocation(actor.getX() + 5, actor.getY());
            }
        }
        
        if(Greenfoot.isKeyDown("up"))
        {
            jump();
        }
    }
    public void jump()
    {
         if(isTouchingGround())
         {
             vSpeed = -14;
             fall();
         }
    }
    private void fall()
    {
         setLocation(getX(), getY() + vSpeed);
         vSpeed = vSpeed + (int)acceleration;
    }
    
    private void checkCollision()
    {
         if(isTouchingGround())
         {
               acceleration = 0;
               vSpeed = 0;
         }
         if(getOneObjectAtOffset(0, -(mario.getHeight() / 2) - 1, Wall.class) != null)
         {
             fall();
             
         }
         else
         {
               acceleration = 1;
         }
    }
    
    public boolean isTouchingGround()
    {
        return getOneObjectAtOffset(0, (mario.getHeight() / 2) + 1, Wall.class) != null;
    }
   
}
