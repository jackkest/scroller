import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class MyWorld here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class MyWorld extends World
{

    /**
     * Constructor for objects of class MyWorld.
     * 
     */
    public MyWorld()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(600, 400, 1, false);
        generateFloor();
        generateCeiling();
        addObject(new Mario(), 300, 300 - 32);
    }
    
    public void generateFloor()
    {
        int i2 = 0;
        for(int i = 0; i < 100; i++)
        {
            addObject(new Wall(), i2, 300);
            i2 = i2 + 32;
        }
    }
    
    public void generateCeiling()
    {
        int i2 = 0;
        for(int i = 0; i < 100; i++)
        {
            addObject(new Wall(), i2, 150);
            i2 = i2 + 32;
        }   
    }
}
